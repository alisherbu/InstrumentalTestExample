package kaa.alisherbu.instrumentaltestexample

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Button
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import kaa.alisherbu.data.database.WordDao
import kaa.alisherbu.data.database.WordEntity
import kaa.alisherbu.data.di.DataComponent
import kaa.alisherbu.data.di.DataComponentImpl
import kaa.alisherbu.instrumentaltestexample.ui.theme.InstrumentalTestExampleTheme
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val dataComponent: DataComponent = DataComponentImpl(applicationContext)
        val wordsDao: WordDao = dataComponent.wordDao
        val mainScope = MainScope()
        enableEdgeToEdge()
        setContent {
            InstrumentalTestExampleTheme {
                Scaffold(modifier = Modifier.fillMaxSize()) { innerPadding ->
                    Column(modifier = Modifier.padding(innerPadding)) {
                        val words by wordsDao.getWordsFlow().collectAsState(emptyList())
                        Row(
                            modifier = Modifier.padding(16.dp),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            var word by remember { mutableStateOf("") }
                            TextField(
                                value = word,
                                onValueChange = { word = it },
                                modifier = Modifier.weight(1f)
                            )
                            Spacer(modifier = Modifier.width(16.dp))
                            Button(
                                onClick = {
                                    mainScope.launch(Dispatchers.IO) {
                                        wordsDao.insert(WordEntity(name = word))
                                    }
                                }
                            ) {
                                Text(text = "Add")
                            }
                        }
                        LazyColumn(
                            modifier = Modifier.padding(16.dp)
                        ) {
                            items(words) {
                                Text(text = it.name)
                            }
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun Greeting(name: String, modifier: Modifier = Modifier) {
    Text(
        text = "Hello $name!",
        modifier = modifier
    )
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    InstrumentalTestExampleTheme {
        Greeting("Android")
    }
}