package kaa.alisherbu.data.database

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import kaa.alisherbu.data.di.DataComponent
import kaa.alisherbu.data.di.TestDataComponent
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test

class WordDaoTest {
    private lateinit var dataComponent: DataComponent
    private lateinit var wordDao: WordDao

    @Before
    fun setUp() {
        val context: Context = ApplicationProvider.getApplicationContext()
        dataComponent = TestDataComponent(context)
        wordDao = dataComponent.wordDao
    }

    @Test
    fun checkIsInserted() = runBlocking {
        val word = WordEntity(name = "abc")
        wordDao.insert(word)
        val words = wordDao.getWordsFlow().first()
        assert(word.name == words[0].name)
    }
}