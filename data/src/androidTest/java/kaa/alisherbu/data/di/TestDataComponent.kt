package kaa.alisherbu.data.di

import android.content.Context
import androidx.room.Room
import kaa.alisherbu.data.database.WordDao
import kaa.alisherbu.data.database.WordDatabase

class TestDataComponent(override val context: Context) : DataComponent {
    private val database: WordDatabase by lazy {
        Room.inMemoryDatabaseBuilder(context, WordDatabase::class.java).build()
    }
    override val wordDao: WordDao = database.wordDao()
}