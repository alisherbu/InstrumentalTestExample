package kaa.alisherbu.data.di

import android.content.Context
import kaa.alisherbu.data.database.WordDao

interface DataComponent {
    val context: Context
    val wordDao: WordDao
}