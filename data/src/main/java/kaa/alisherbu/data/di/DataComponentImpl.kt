package kaa.alisherbu.data.di

import android.content.Context
import androidx.room.Room
import kaa.alisherbu.data.database.WordDao
import kaa.alisherbu.data.database.WordDatabase

class DataComponentImpl(override val context: Context) : DataComponent {
    private val database: WordDatabase by lazy {
        Room.databaseBuilder(context, WordDatabase::class.java, "word_database").build()
    }
    override val wordDao: WordDao = database.wordDao()
}