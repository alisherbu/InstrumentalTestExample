package kaa.alisherbu.data.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "word")
data class WordEntity(
    @PrimaryKey(autoGenerate = true) @ColumnInfo("id")
    val id: Int = 0,
    @ColumnInfo("name")
    val name: String
)